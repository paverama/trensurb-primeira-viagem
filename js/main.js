var Application =
{
	init:function()
	{
		//Insert browser name/version on the html tag
		Utils.detectBrowser();

		//Initializations
		Player.init();
		Camera.init();
		Environment.init();
		Timeline.init();
		Modals.init();
		Stations.init();

		//Window resize event
		$(window).resize(Application.onWindowResize);

		//Body mousedown event
		$('html, body').bind('mousedown', Application.onBodyMouseDown);

		//Keydown event
		$(window).bind('keydown', Application.onWindowKeyDown);
	},

	onWindowResize:function()
	{
		Camera.updatePosition();
	},

	onBodyMouseDown:function( pEvt )
	{
		//Prevent middle click scroll
		pEvt.preventDefault();
	},

	onWindowKeyDown:function( pEvt )
	{
		if (Stations.isActive == true) return;

		var keyCode = pEvt.keyCode;

		if (keyCode == 39)
		{
			if ((Modals.current != undefined)
			 && (Modals.isAnimating == false)
			 && (Modals.current != Timeline.LABEL_FIM))
			{
				Modals.hide();
			}
		}

		return false;
	}
}

var Player = 
{
	ANIM_IDLE_RIGHT:'idleRight',
	ANIM_IDLE_LEFT:'idleLeft',
	ANIM_WALK_RIGHT:'walkRight',
	ANIM_WALK_LEFT:'walkLeft',

	content:undefined,

	init:function()
	{
		Player.content = $('#player');
		Player.content.animateSprite(
		{
			columns:5,
			totalFrames:5,
			duration:1000,
			loop:true,
			animations:
			{
				idleRight:[0],
				idleLeft:[1],
				walkRight:[2,3,4,5],
				walkLeft:[6,7,8,9]
			}
		});
	},

	setPosition:function( pX, pY )
	{
		TweenMax.set(Player.content, { x:pX, y:pY });
	},

	idle:function()
	{
		Player.content.animateSprite('play', 'idleRight');
	},

	walk:function()
	{
		Player.content.animateSprite('play', 'walkRight');
	}
}

var Camera = 
{
	content:undefined,
	lookAt:undefined,

	init:function()
	{
		Camera.content = $('html, body');

		Camera.lookAt = $('#camera');

		$('#content').scrollLeft(0);
		$('#content').scrollTop(0);
	},

	setPosition:function( pX, pY )
	{
		TweenMax.set(Camera.lookAt, { x:pX, y:pY });
		Camera.updatePosition();
	},

	getPosition:function()
	{
		var data = { x:0, y:0 };

		data.x = $('html').scrollLeft();
		if (data.x == 0) data.x = $('body').scrollLeft();

		data.y = $('html').scrollTop();
		if (data.y == 0) data.y = $('body').scrollTop();

		return data;
	},

	updatePosition:function()
	{
		var winWidth = $(window).width();
		var winHeight = $(window).height();
		var camLeft = GreenProp.x(Camera.lookAt);
		var camTop = GreenProp.y(Camera.lookAt);

		var posX = (camLeft - (winWidth*.5));
		var posY = (camTop - (winHeight*.5));

		Camera.content.scrollLeft(posX);
		Camera.content.scrollTop(posY);
	}
}

var Environment = 
{
	content:undefined,

	train01:undefined,
	train02:undefined,
	train03:undefined,
	train04:undefined,
	train05:undefined,

	init:function()
	{
		Environment.content = $('#environment');

		Environment.train01 = Environment.content.find('#train-1');
		Environment.train02 = Environment.content.find('#train-2');
		Environment.train03 = Environment.content.find('#train-3');
		Environment.train04 = Environment.content.find('#train-4');
		Environment.train05 = Environment.content.find('#train-5');
	}
}

var Stations = 
{
	content:undefined,
	isActive:false,
	scrollTween:undefined,

	keysImage:undefined,
	continueButton:undefined,

	init:function()
	{
		Stations.content = $('#stations');

		Stations.keysImage = Stations.content.find('.keys');
		Stations.continueButton = Stations.content.find('.continue');
		Stations.continueButton.bind('click', function()
		{
			TweenMax.to(Camera.lookAt, .5, { x:'+=4', ease:Sine.easeIn, onUpdate:Camera.updatePosition });
			setTimeout(function(){ Stations.hide() }, 250);
			return false;
		});
		Stations.hide(false);

		$('#map a.link-modal').bind('click', function()
		{
			var id = $(this).attr('data-modal-id');
			Modals.show(id, Modals.TYPE_STATION);
			return false;
		});

		//Keydown event
		$(window).bind('keydown', Stations.onWindowKeyDown);
	},

	onWindowKeyDown:function( pEvt )
	{
		if ( (Stations.isActive == false) || (Modals.isAnimating == true) ) return;

		var keyCode = pEvt.keyCode;

		if (Modals.current != undefined)
		{
			Modals.hide();
			return;
		}

		if (keyCode == 39) Stations.scrollRight();
		else if (keyCode == 37) Stations.scrollLeft();
	},

	show:function()
	{
		Timeline.pause();

		Stations.isActive = true;

		Modals.show('estacoes', Modals.TYPE_TRAIN);

		var time = .5;
		TweenMax.to(Stations.keysImage, time, 			{ scale:1, ease:Back.easeOut });
		TweenMax.to(Stations.continueButton, time, 		{ scale:1, ease:Back.easeOut });
	},

	hide:function( pAnimate )
	{
		Timeline.play();

		Stations.isActive = false;

		var time = pAnimate == true ? .5 : 0;
		TweenMax.to(Stations.keysImage, time, 			{ scale:0, ease:Back.easeIn });
		TweenMax.to(Stations.continueButton, time, 		{ scale:0, ease:Back.easeIn });
	},

	scrollRight:function()
	{
		var scrollRight = Camera.getPosition().x + $(window).width();
		var mapRight = $('#map').offset().left + $('#map').width();

		var posX = 500;
		if ( (scrollRight + posX) > mapRight ) posX = mapRight - scrollRight;

		if (Stations.scrollTween != undefined) Stations.scrollTween.kill();
		Stations.scrollTween = TweenMax.to(Camera.lookAt, .5, { x:'+='+posX, ease:Sine.easeOut, onUpdate:Camera.updatePosition });
	},

	scrollLeft:function()
	{
		var scrollLeft = Camera.getPosition().x;
		var mapLeft = $('#map').offset().left;

		var posX = 500;
		if ( (scrollLeft - posX) < mapLeft ) posX = scrollLeft - mapLeft;

		if (Stations.scrollTween != undefined) Stations.scrollTween.kill();
		Stations.scrollTween = TweenMax.to(Camera.lookAt, .5, { x:'-='+posX, ease:Sine.easeOut, onUpdate:Camera.updatePosition });
	}
}

var Timeline = 
{
	LABEL_INICIO:'label-inicio',
	LABEL_FAXINEIRA:'label-faxineira',
	LABEL_BILHETEIRO:'label-bilheteiro',
	LABEL_ESCADA_ROLANTE:'label-escada-rolante',
	LABEL_FOME:'label-fome',
	LABEL_LINHA_AMARELA:'label-linha-amarela',
	LABEL_NAO_SE_PERCA:'label-nao-se-perca',
	LABEL_SEGURANCA:'label-seguranca',
	LABEL_OPERADOR:'label-operador',
	LABEL_ESTACOES:'label-estacoes',

	LABEL_ESTACAO_MERCADO:'label-estacao-mercado',
	LABEL_ESTACAO_RODOVIARIA:'label-estacao-rodoviaria',
	LABEL_ESTACAO_SAO_PEDRO:'label-estacao-sao-pedro',
	LABEL_ESTACAO_FARRAPOS:'label-estacao-farrapos',
	LABEL_ESTACAO_AEROPORTO:'label-estacao-aeroporto',
	LABEL_ESTACAO_ANCHIETA:'label-estacao-anchieta',
	LABEL_ESTACAO_NITEROI:'label-estacao-niteroi',
	LABEL_ESTACAO_FATIMA:'label-estacao-fatima',
	LABEL_ESTACAO_CANOAS:'label-estacao-canoas',
	LABEL_ESTACAO_MATHIAS_VELHO:'label-estacao-mathias-velho',
	LABEL_ESTACAO_SAO_LUIZ:'label-estacao-sao-luiz',
	LABEL_ESTACAO_PETROBRAS:'label-estacao-petrobras',
	LABEL_ESTACAO_ESTEIO:'label-estacao-esteio',
	LABEL_ESTACAO_LUIZ_PASTEUR:'label-estacao-luiz-pasteur',
	LABEL_ESTACAO_SAPUCAIA:'label-estacao-sapucaia',
	LABEL_ESTACAO_UNISINOS:'label-estacao-unisinos',
	LABEL_ESTACAO_SAO_LEOPOLDO:'label-estacao-sao-leopoldo',
	LABEL_ESTACAO_RIO_DOS_SINOS:'label-estacao-rio-dos-sinos',
	LABEL_ESTACAO_SANTO_AFONSO:'label-estacao-santo-afonso',
	LABEL_ESTACAO_INDUSTRIAL:'label-estacao-industrial',
	LABEL_ESTACAO_FENAC:'label-estacao-fenac',
	LABEL_ESTACAO_NOVO_HAMBURGO:'label-estacao-novo-hamburgo',

	LABEL_FONE_DE_OUVIDO:'label-fone-de-ouvido',
	LABEL_LIXEIRA:'label-lixeira',
	LABEL_PORTA:'label-porta',
	LABEL_BANCO:'label-banco',
	LABEL_JANELA:'label-janela',
	LABEL_FIM:'label-fim',

	timeline:undefined,
	time:0,

	init:function()
	{
		var scope = $(Environment.content).find('#resolution-4x');

		var player = Player.content;
		var camera = Camera.lookAt;
		var train01 = Environment.train01;
		var train02 = Environment.train02;
		var train03 = Environment.train03;
		var train04 = Environment.train04;
		var train05 = Environment.train05;

		var time = 0;
		var delay = 0;
		var label = undefined;

		//Camera and Player start position
		Camera.setPosition(700, 1000);
		Player.setPosition(-130, 600);

		var tml = Timeline.timeline = new TimelineMax({ paused:false, onUpdate:Camera.updatePosition });
		
		//Inicio
		label = Timeline.LABEL_INICIO;
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_START]);
		tml.addLabel(label);

		//Faxineira
		label = Timeline.LABEL_FAXINEIRA;
		tml.addCallback(Player.walk, '+=.1');
		tml.to(player, 2.0,		{ x:'+=550', y:'+=0', ease:Linear.easeNone }, 'faxineira');
		tml.to(player, 1.8,		{ x:'+=400', y:'+=400', ease:Linear.easeNone });
		tml.to(player, 1.8,		{ x:'+=380', y:'+=0', ease:Linear.easeNone });
		tml.addCallback(Player.idle);
		tml.to(camera, 5.6,		{ x:'+=800', y:'+=0', ease:Sine.easeInOut }, 'faxineira');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_RETRO]);
		tml.addCallback(function(){Modals.show(Timeline.LABEL_FAXINEIRA)});
		tml.addLabel(label);

		//Bilheteiro
		label = Timeline.LABEL_BILHETEIRO;
		tml.addCallback(Player.walk, '+=.1');
		tml.to(player, 1.4,		{ x:'+=200', y:'-=200', ease:Linear.easeNone }, 'bilheteiro');
		tml.addCallback(Player.idle);
		tml.to(camera, 1.4,		{ x:'+=200', y:'-=100', ease:Sine.easeInOut }, 'bilheteiro');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_RETRO]);
		tml.addLabel(label);

		//Escada Rolante
		label = Timeline.LABEL_ESCADA_ROLANTE;
		tml.addCallback(Player.walk, '+=.1');
		tml.to(player, 1.8,		{ x:'+=500', y:'+=75', ease:Linear.easeNone }, 'escada-rolante');
		tml.to(player, 2.8,		{ x:'+=950', y:'+=0', ease:Linear.easeNone });
		tml.to(camera, 4.6,		{ x:'+=1500', y:'-=100', ease:Sine.easeInOut }, 'escada-rolante');
		tml.addCallback(Player.idle);
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_TRAIN]);
		tml.addLabel(label);

		//Fome
		label = Timeline.LABEL_FOME;
		tml.to(player, 2.8,		{ x:'+=430', y:'-=530', ease:Linear.easeNone }, 'fome');
		tml.addCallback(Player.walk);
		tml.to(player, 2.0,		{ x:'+=300', y:'+=0', ease:Linear.easeNone });
		tml.to(camera, 4.8,		{ x:'+=500', y:'-=300', ease:Sine.easeInOut }, 'fome');
		tml.addCallback(Player.idle);
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_TRAIN]);
		tml.addLabel(label);

		//Linha Amarela
		label = Timeline.LABEL_LINHA_AMARELA;
		tml.addCallback(Player.walk, '+=.1');
		tml.to(player, 1.8,		{ x:'+=525', y:'+=0', ease:Linear.easeNone }, 'linha-amarela');
		tml.to(camera, 1.8,		{ x:'+=800', y:'+=0', ease:Sine.easeInOut }, 'linha-amarela');
		tml.addCallback(Player.idle);
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_TRAIN]);
		tml.addLabel(label);

		//Não se Perca
		label = Timeline.LABEL_NAO_SE_PERCA;
		tml.addCallback(Player.walk, '+=.1');
		tml.to(player, 1.4,		{ x:'+=510', y:'-=0', ease:Linear.easeNone }, 'nao-se-perca');
		tml.to(camera, 1.4,		{ x:'+=600', y:'+=0', ease:Sine.easeInOut }, 'nao-se-perca');
		tml.addCallback(Player.idle);
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_TRAIN]);
		tml.addLabel(label);

		//Seguranca
		label = Timeline.LABEL_SEGURANCA;
		tml.addCallback(Player.walk, '+=.1');
		tml.to(player, 1.4,		{ x:'+=510', y:'+=20', ease:Linear.easeNone }, 'seguranca');
		tml.to(camera, 1.4,		{ x:'+=300', y:'+=0', ease:Sine.easeInOut }, 'seguranca');
		tml.addCallback(Player.idle);
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_RETRO]);
		tml.addLabel(label);

		//Operador
		label = Timeline.LABEL_OPERADOR;
		tml.addCallback(Player.walk, '+=.1');
		tml.to(player, 1.0,		{ x:'+=200', y:'+=150', ease:Linear.easeNone }, 'operador');
		tml.addCallback(Player.idle);
		tml.set(player, 		{ visibility:'hidden' }, 'operador+=1');
		tml.to(train02, 5.0,	{ x:'-=15000', y:'+=0', ease:Sine.easeOut }, 'operador+=7');
		tml.to(train01, 12.0,	{ x:'+=8000', y:'+=0', ease:Sine.easeInOut }, 'operador+=1');
		tml.to(camera, 12.0,	{ x:'+=10650', y:'-=150', ease:Sine.easeInOut }, 'operador+=1');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_RETRO]);
		tml.addLabel(label);

		//Estacoes
		label = Timeline.LABEL_ESTACOES;
		tml.to(camera, 10.0,	{ x:'+=9700', y:'+=310', ease:Sine.easeInOut }, 'estacoes');
		tml.to(train01, 10.0,	{ x:'+=6000', y:'+=0', ease:Sine.easeInOut }, 'estacoes');
		tml.addCallback(Stations.show, '+=0');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_TRAIN]);
		tml.addLabel(label);

		/*
		//Estacao Mercado
		label = Timeline.LABEL_ESTACAO_MERCADO;
		tml.to(camera, 10.0,	{ x:'+=9700', y:'+=310', ease:Sine.easeInOut }, 'estacao-mercado');
		tml.to(train01, 10.0,	{ x:'+=6000', y:'+=0', ease:Sine.easeInOut }, 'estacao-mercado');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao Rodoviária
		label = Timeline.LABEL_ESTACAO_RODOVIARIA;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-rodoviaria');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao São Pedro
		label = Timeline.LABEL_ESTACAO_SAO_PEDRO;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-sao-pedro');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao Farrapos
		label = Timeline.LABEL_ESTACAO_FARRAPOS;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-farrapos');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao Aeroporto
		label = Timeline.LABEL_ESTACAO_AEROPORTO;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-aeroporto');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao Anchieta
		label = Timeline.LABEL_ESTACAO_ANCHIETA;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-anchieta');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao Niterói
		label = Timeline.LABEL_ESTACAO_NITEROI;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-niteroi');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao Fátima
		label = Timeline.LABEL_ESTACAO_FATIMA;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-fatima');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao Canoas
		label = Timeline.LABEL_ESTACAO_CANOAS;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-canoas');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao Mathias Velho
		label = Timeline.LABEL_ESTACAO_MATHIAS_VELHO;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-mathias-velho');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao São Luiz
		label = Timeline.LABEL_ESTACAO_SAO_LUIZ;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-sao-luiz');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao Petrobras
		label = Timeline.LABEL_ESTACAO_PETROBRAS;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-petrobras');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao Esteio
		label = Timeline.LABEL_ESTACAO_ESTEIO;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-esteio');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao Luiz Pasteur
		label = Timeline.LABEL_ESTACAO_LUIZ_PASTEUR;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-luiz-pasteur');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao Sapucaia
		label = Timeline.LABEL_ESTACAO_SAPUCAIA;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-sapucaia');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao Unisinos
		label = Timeline.LABEL_ESTACAO_UNISINOS;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-unisinos');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao São Leopoldo
		label = Timeline.LABEL_ESTACAO_SAO_LEOPOLDO;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-sao-leopoldo');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao Rio dos Sinos
		label = Timeline.LABEL_ESTACAO_RIO_DOS_SINOS;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-rio-dos-sinos');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao Santo Afonso
		label = Timeline.LABEL_ESTACAO_SANTO_AFONSO;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-santo-afonso');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao Industrial
		label = Timeline.LABEL_ESTACAO_INDUSTRIAL;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-industrial');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao Fenac
		label = Timeline.LABEL_ESTACAO_FENAC;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-fenac');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);

		//Estacao Novo Hamburgo
		label = Timeline.LABEL_ESTACAO_NOVO_HAMBURGO;
		tml.to(camera, 1.0,	{ x:'+=100', y:'+=0', ease:Sine.easeInOut }, 'estacao-novo-hamburgo');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_STATION]);
		tml.addLabel(label);
		*/

		//Fone de Ouvido
		label = Timeline.LABEL_FONE_DE_OUVIDO;
		tml.addCallback(Player.walk, '+=.1');
		tml.set(player, 		{ visibility:'visible', x:39550, y:'+=50', scale:1.7 });
		//tml.to(camera, 5.5,		{ x:'+=12400', y:'+=0', ease:Sine.easeInOut }, 'fone-de-ouvido');
		tml.to(camera, 5.5,		{ x:40250, y:660, ease:Sine.easeInOut }, 'fone-de-ouvido');
		tml.to(player, 1.4,		{ x:'+=170', y:'+=20', ease:Linear.easeNone }, '-=1');
		tml.addCallback(Player.idle);
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_TRAIN]);
		tml.addLabel(label);

		//Lixeira
		label = Timeline.LABEL_LIXEIRA;
		tml.addCallback(Player.walk, '+=.1');
		tml.to(player, 2.0,		{ x:'+=740', y:'+=90', ease:Linear.easeNone }, 'lixeira');
		tml.addCallback(Player.idle);
		tml.to(camera, 3.0,		{ x:'+=600', y:'+=0', ease:Sine.easeInOut }, 'lixeira');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_TRAIN]);
		tml.addLabel(label);

		//Porta
		label = Timeline.LABEL_PORTA;
		tml.addCallback(Player.walk, '+=.1');
		tml.to(player, 3.0,		{ x:'+=830', y:'-=120', ease:Linear.easeNone }, 'porta');
		tml.addCallback(Player.idle);
		tml.to(camera, 3.0,		{ x:'+=870', y:'+=0', ease:Sine.easeInOut }, 'porta');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_TRAIN]);
		tml.addLabel(label);

		//Banco
		label = Timeline.LABEL_BANCO;
		tml.addCallback(Player.walk, '+=.1');
		tml.to(player, 3.0,		{ x:'+=950', y:'+=50', ease:Linear.easeNone }, 'banco');
		tml.addCallback(Player.idle);
		tml.to(camera, 3.0,		{ x:'+=920', y:'+=0', ease:Sine.easeInOut }, 'banco');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_TRAIN]);
		tml.addLabel(label);

		//Janela
		label = Timeline.LABEL_JANELA;
		tml.addCallback(Player.walk, '+=.1');
		tml.to(player, 3.0,		{ x:'+=830', y:'-=50', ease:Linear.easeNone }, 'janela');
		tml.addCallback(Player.idle);
		tml.to(camera, 3.0,		{ x:'+=700', y:'+=0', ease:Sine.easeInOut }, 'janela');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_TRAIN]);
		tml.addLabel(label);

		//Fim
		label = Timeline.LABEL_FIM;
		tml.to(player, 8.0,		{ x:'+=40000', y:'+=0', ease:Sine.easeIn }, 'fim');
		tml.to(train03, 8.0,	{ x:'+=40000', y:'+=0', ease:Sine.easeIn }, 'fim');
		tml.set(train03, 		{ visibility:'hidden' });
		tml.set(player, 		{ visibility:'hidden' });
		tml.to(train05, 8.0,	{ x:'-=8000', y:'+=0', ease:Linear.easeNone }, 'fim-=2');
		tml.to(train04, 8.0,	{ x:'+=17750', y:'+=0', ease:Sine.easeOut }, '-=1');
		tml.set(player, 		{ visibility:'visible', scale:1, x:'79000', y:'475' });
		tml.addCallback(Player.walk);
		tml.to(player, 1.4,		{ x:'+=100', y:'-=120', ease:Linear.easeNone });
		tml.addCallback(Player.idle);
		tml.to(train04, 2.5,	{ x:'+=2000', y:'+=0', ease:Sine.easeIn }, '-=1');
		tml.to(camera, 15.0,	{ x:'+=35800', y:'+=0', ease:Sine.easeInOut }, 'fim');
		tml.addCallback(Modals.show, '+=0', [label, Modals.TYPE_END]);
		tml.addLabel(label);

		//Timeline.gotoLabel(Timeline.LABEL_OPERADOR);
		//Timeline.gotoLabel(Timeline.LABEL_JANELA);
		//Timeline.timeline.seek(101);
	},

	play:function()
	{
		Timeline.timeline.play();
	},

	pause:function()
	{
		Timeline.timeline.pause();
	},

	gotoLabel:function( pLabel )
	{
		Timeline.timeline.seek(pLabel);
	}
}

var Modals =
{
	TYPE_START:'modal-start',
	TYPE_RETRO:'modal-retro',
	TYPE_TRAIN:'modal-trem',
	TYPE_STATION:'modal-station',
	TYPE_END:'modal-end',

	content:undefined,
	scope:undefined,

	isAnimating:false,

	current:undefined,
	currentType:undefined,

	init:function()
	{
		Modals.content = $('#modals');

		Modals.content.find('a.modal-close-button').bind('click', function()
		{
			Modals.hide();
			return false;
		});
		Modals.content.find('a.modal-restart').bind('click', function()
		{
			window.location.href = window.location.href;
			return false;
		});
	},

	show:function( pId, pType )
	{
		Timeline.pause();

		Modals.current = pId;

		var id = '#' + pId.replace('label-', '');
		var type = pType;
		Modals.currentType = type;

		console.log('Modals.show() >> id:' + id + ' - type:' + type);

		Modals.content.show();
		Modals.content.find('.content').css('display', 'none');
		Modals.content.find(id).css('display', 'block');

		var content = Modals.content;
		var scope = Modals.content.find(id);
		Modals.scope = scope;
		Modals.isAnimating = true;

		var tml = new TimelineMax({ onComplete:function(){ Modals.isAnimating = false } });
		tml.timeScale(2);
		tml.fromTo(content.find('.base'), 1, 							{ opacity:0, y:250, height:0 }, { opacity:1, y:0, height:'100%', ease:Cubic.easeOut });
		tml.fromTo(content.find('#modal-logo'), 1, 						{ scale:0 }, { scale:1, ease:Back.easeOut }, '-=.5');

		switch(type)
		{
			case Modals.TYPE_START :
				tml.fromTo(content.find('.modal-header'), 1, 			{ scale:0 }, { scale:1, ease:Back.easeOut }, '-=.5');
				tml.fromTo(content.find('.modal-content'), .75, 		{ opacity:0, y:50 }, { opacity:1, y:0, ease:Cubic.easeOut }, '-=.5');
				tml.fromTo(content.find('.modal-info'), .5, 			{ opacity:0, y:50 }, { opacity:1, y:0, ease:Cubic.easeOut }, '-=.5');
				break;

			case Modals.TYPE_RETRO :
				tml.fromTo(content.find('.modal-header'), 1, 			{ scale:0 }, { scale:1, ease:Back.easeOut }, '-=.5');
				tml.fromTo(content.find('.modal-title'), 1,			{ scale:0 }, { scale:1, ease:Back.easeOut }, '-=.5');
				tml.fromTo(content.find('.modal-content'), .75, 		{ opacity:0, y:50 }, { opacity:1, y:0, ease:Cubic.easeOut }, '-=.5');
				tml.fromTo(content.find('.modal-animation-gif'), 1,		{ scale:0 }, { scale:1, ease:Back.easeOut }, '-=.5');
				tml.fromTo(content.find('.modal-info'), .5, 			{ opacity:0, y:50 }, { opacity:1, y:0, ease:Cubic.easeOut }, '-=.5');
				tml.fromTo(content.find('.modal-close-button'), 1,		{ scale:0 }, { scale:1, ease:Back.easeOut }, '-=.5');
				break;

			case Modals.TYPE_TRAIN :
				tml.fromTo(content.find('.modal-zurbinho'), 1, 			{ scale:0 }, { scale:1, ease:Back.easeOut }, '-=.5');
				tml.fromTo(content.find('.modal-text'), 1, 				{ scale:0, x:-200, y:100 }, { scale:1, x:0, y:0, ease:Back.easeOut }, '-=.5');
				tml.fromTo(content.find('.modal-animation-gif'), 1,		{ scale:0 }, { scale:1, ease:Back.easeOut }, '-=.5');
				break;

			case Modals.TYPE_END :
				tml.fromTo(content.find('.modal-zurbinho'), 1, 			{ scale:0 }, { scale:1, ease:Back.easeOut }, '-=.5');
				tml.fromTo(content.find('.modal-content'), .75, 		{ opacity:0, scale:0, x:-200, y:100 }, { opacity:1, scale:1, x:0, y:0, ease:Back.easeOut }, '-=.5');
				tml.fromTo(content.find('.modal-credits'), .5, 			{ opacity:0 }, { opacity:1, ease:Linear.easeNone }, '-=.35');
				tml.fromTo(content.find('.modal-restart'), 1, 			{ scale:0 }, { scale:1, ease:Back.easeOut }, '-=.25');
				break;

            case Modals.TYPE_STATION :
				tml.fromTo(content.find('.modal-title'), 1,			{ scale:0 }, { scale:1, ease:Back.easeOut }, '-=.5');
				tml.fromTo(content.find('.modal-content'), .75, 		{ opacity:0, y:50 }, { opacity:1, y:0, ease:Cubic.easeOut }, '-=.5');
				tml.fromTo(content.find('.modal-close-button'), 1,		{ scale:0 }, { scale:1, ease:Back.easeOut }, '-=.5');
				console.log(content.find('.modal-content').height());
				//content.find('.modal-cloud').css('top', posTop - 20);
				tml.fromTo(content.find('.modal-cloud'), .75, 		{ opacity:0, x: 50 }, { opacity:1, x: 0, ease:Cubic.easeOut }, '-=.5');
                break;
		}

		if (Modals.current != Timeline.LABEL_FIM)
		{
			tml.from(content.find('#keys'), 1, 							{ scale:0, ease:Back.easeOut }, '-=.5');
		}
		else
		{
			TweenMax.set(content.find('#keys'), { autoAlpha:0 });
		}
	},

	hide:function()
	{
		if (Stations.isActive == false)
		{
			Timeline.play();
		}

		Modals.current = undefined;

		var content = Modals.content;
		var scope = Modals.scope;
		var type = Modals.currentType;
		Modals.isAnimating = true;

		var tml = new TimelineMax({ 
			onComplete:function(){ 
				Modals.isAnimating = false 
				Modals.content.hide();
			} 
		});

		tml.timeScale(4.5);
		tml.to(content.find('#keys'), 1, 						{ scale:0, ease:Back.easeIn });

		switch(type)
		{
			case Modals.TYPE_START :
				tml.to(content.find('.modal-info'), .5, 		{ opacity:0, y:0, ease:Cubic.easeIn }, '-=.5');
				tml.to(content.find('.modal-content'), .75, 	{ opacity:0, y:0, ease:Cubic.easeIn }, '-=.25');
				tml.to(content.find('.modal-header'), 1, 		{ scale:0, ease:Back.easeIn }, '-=.35');
				break;

			case Modals.TYPE_RETRO :
				tml.to(content.find('.modal-close-button'), 1,	{ scale:0, ease:Back.easeIn }, '-=.5');
				tml.to(content.find('.modal-info'), .5, 		{ opacity:0, y:50, ease:Cubic.easeIn }, '-=.5');
				tml.to(content.find('.modal-animation-gif'), 1,	{ scale:0, ease:Back.easeIn }, '-=.5');
				tml.to(content.find('.modal-content'), .75, 	{ opacity:0, y:50, ease:Cubic.easeIn }, '-=.5');
				tml.to(content.find('.modal-title'), 1, 		{ scale:0, ease:Back.easeIn }, '-=.5');
				tml.to(content.find('.modal-header'), 1, 		{ scale:0, ease:Back.easeIn }, '-=.5');
				break;

			case Modals.TYPE_TRAIN :
				tml.to(content.find('.modal-zurbinho'), 1, 		{ scale:0, ease:Back.easeIn }, '-=.5');
				tml.to(content.find('.modal-text'), 1, 			{ scale:0, x:-200, y:100, ease:Back.easeIn }, '-=.5');
				tml.to(content.find('.modal-animation-gif'), 1,	{ scale:0, ease:Back.easeIn }, '-=.5');
				break;

			case Modals.TYPE_END :
				tml.to(content.find('.modal-restart'), 1, 		{ scale:0, ease:Back.easeIn }, '-=.25');
				tml.to(content.find('.modal-credits'), .5, 		{ opacity:0, ease:Linear.easeNone }, '-=.35');
				tml.to(content.find('.modal-content'), .75, 	{ scale:0, x:-200, y:100, ease:Back.easeIn }, '-=.5');
				tml.to(content.find('.modal-zurbinho'), 1, 		{ scale:0, ease:Back.easeIn }, '-=.5');
				break;

			 case Modals.TYPE_STATION :
			 	tml.to(content.find('.modal-close-button'), 1,	{ scale:0, ease:Back.easeIn }, '-=.5');
				tml.to(content.find('.modal-content'), .75, 	{ opacity:0, y:50, ease:Cubic.easeIn }, '-=.5');
				tml.to(content.find('.modal-title'), 1, 		{ scale:0, ease:Back.easeIn }, '-=.5');
				tml.to(content.find('.modal-cloud'), .75, 	{ opacity:0, x:50, ease:Cubic.easeIn }, '-=.5');
                break;
		}

		tml.to(content.find('#modal-logo'), 1, 					{ scale:0, ease:Back.easeIn }, '-=.5');
		tml.to(content.find('.base'), 1, 						{ opacity:0, y:250, height:0, ease:Cubic.easeIn }, '-=.5');
	}
}

var Utils =
{
	detectBrowser:function() 
	{
		var name = bowser.name.toLowerCase();
		$('html').addClass(name); 

		var version = String(name + Math.floor(bowser.version)).toLowerCase();
		$('html').addClass(version);
	}
}

$(document).ready(Application.init);