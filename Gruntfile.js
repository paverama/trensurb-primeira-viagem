module.exports = function(grunt) { 

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        compass: {
            options: {
                cssDir: 'css/',
                sassDir: 'scss/',
                imagesDir: 'images/',
                fontsDir: 'fonts/',
                relativeAssets: true
            },

            dev: {
                options: {
                    environment: 'development',
                    outputStyle: 'expanded'    
                }
            },

            build: {
                options: {
                    environment: 'production',
                    force: true
                }
            }
        },

        watch: {
            scss: {
                files: ['scss/**/*.scss'],
                tasks: ['compass:dev'],
                options: {
                    spawn: false,
                },
            },

            livereload: {
                options: { 
                    livereload: true 
                },

                files: ['css/main.css', 'index.html', 'Gruntfile.js'],
            },
        }
    });

    grunt.registerTask('default', ['watch']);
};